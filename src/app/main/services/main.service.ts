import { HttpClient } from '@angular/common/http';
import { catchError } from "rxjs/operators";
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Pessoa } from '../models/pessoa';
import { Produto } from '../models/produto';


@Injectable({
  providedIn: 'root'
})
export class MainService {

  readonly url = 'http://localhost:3000/api';

  constructor(private http: HttpClient) { }

  getPessoa(): Observable<Pessoa[]> {
    return this.http.get<Pessoa[]>(this.url + '/people')
    .pipe(
      catchError((e)=> {
        console.log(e);
        return throwError(e);
      })
    )
  }

  getProduto(): Observable<Produto[]> {
    return this.http.get<Produto[]>(this.url + '/products')
    .pipe(
      catchError((e)=> {
        console.log(e);
        return throwError(e);
      })
    )
  }
}
