export interface Produto {
    name: string;
    department: string;
    price: number;
}
