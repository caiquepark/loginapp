import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Produto } from '../../models/produto';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.sass']
})
export class ProdutoComponent implements OnInit {

  constructor(private mainService: MainService) { }

  produtos: Produto[] = [];

  ngOnInit(): void {
     this.mainService.getProduto()
    .subscribe(
      response => this.produtos = response
    )
    
  }

}
