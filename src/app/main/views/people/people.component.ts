import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Pessoa } from '../../models/pessoa';
import { MainService } from '../../services/main.service';

@Component({
  selector: 'app-people',
  templateUrl: './people.component.html',
  styleUrls: ['./people.component.sass']
})
export class PeopleComponent implements OnInit {

   pessoas: Pessoa[] = []; 

  constructor(private mainService: MainService) { }

  ngOnInit(): void {
    this.mainService.getPessoa()
    .subscribe(
      response => this.pessoas = response
    )
    
  }

  


}
