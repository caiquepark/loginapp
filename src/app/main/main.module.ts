import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { PeopleComponent } from './views/people/people.component';
import { ProdutoComponent } from './views/produto/produto.component';




@NgModule({
  declarations: [
   
  
    PeopleComponent,
    ProdutoComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
   

  ]
})
export class MainModule { }
