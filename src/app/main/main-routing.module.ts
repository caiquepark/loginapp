import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PeopleComponent } from './views/people/people.component';
import { ProdutoComponent } from './views/produto/produto.component';

const routes: Routes = [
  {path: '', redirectTo: 'people'},
  {path: 'people', component: PeopleComponent},
  {path: 'produto', component: ProdutoComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
