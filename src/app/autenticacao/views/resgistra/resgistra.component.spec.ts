import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ResgistraComponent } from './resgistra.component';

describe('ResgistraComponent', () => {
  let component: ResgistraComponent;
  let fixture: ComponentFixture<ResgistraComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ResgistraComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ResgistraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
