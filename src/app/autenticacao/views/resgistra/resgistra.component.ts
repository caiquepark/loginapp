import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { timeout } from 'rxjs/operators';
import { Usuario } from '../../models/usuario';
import { AutenticaService } from '../../services/autentica.service';

@Component({
  selector: 'app-resgistra',
  templateUrl: './resgistra.component.html',
  styleUrls: ['./resgistra.component.sass']
})
export class ResgistraComponent implements OnInit {

  formRegister = this.fb.group({
    firstName: ['', [Validators.required]],
    lastName: ['', [Validators.required]],
    address: ['', [Validators.required]],
    city: ['', [Validators.required]],
    state: ['', [Validators.required]],
    phone: [0, [Validators.required, Validators.minLength(10)]],
    mobilePhone: [0, [Validators.required, Validators.maxLength(11)]],
    email: ['',[Validators.required, Validators.email]],
    password: ['',[Validators.required, Validators.minLength(6)]],
    password2: ['',[Validators.required, Validators.minLength(6)]]

    
  }, {validator: this.verificaSenha})
  states = ['MG', 'RS', 'BA', 'MT', 'SP', 'RJ', 'MA', 'CE', ]
 

  constructor(
    private fb: FormBuilder,
    private autenticaService: AutenticaService, 
    private snackBar: MatSnackBar,
    private router: Router,
    ) { }

  ngOnInit(): void {
  }

    verificaSenha(group: FormGroup) {
        if(group){
          const password = group.controls['password'].value;
          const password2 = group.controls['password2'].value;
          if (password == password2) {
            return null;
          }
        }
        return {matchin: false};
    }

    onSubmit() {
    
      let u: Usuario = {
        ...this.formRegister.value,
         password: this.formRegister.value.password};
      this.autenticaService.register(u)
      .subscribe(
        (u) => {
          this.snackBar.open('successfuly register. Use  your credentials to sing in'
          , 'OK', {duration: 2000})
          this.router.navigateByUrl('autenticacao/login');
          
        },
        (err) => {
          console.error(err);
          this.snackBar.open(err.error.message, 'OK', {duration: 2000})
          
        }
      )
      
    }
}
