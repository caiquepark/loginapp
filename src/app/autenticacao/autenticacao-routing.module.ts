import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './views/login/login.component';
import { ResgistraComponent } from './views/resgistra/resgistra.component';

const routes: Routes = [
  {path: 'autenticacao/login', component: LoginComponent },
  {path: 'autenticacao/registra', component: ResgistraComponent},
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AutenticacaoRoutingModule { }
