import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AutenticacaoRoutingModule } from './autenticacao-routing.module';
import { ValidaComponent } from './views/valida/valida.component';
import { ResgistraComponent } from './views/resgistra/resgistra.component';
import { LoginComponent } from './views/login/login.component';
import { MaterialModule } from '../material.module';
import { ReactiveFormsModule } from '@angular/forms';
import { AuthInterceptor } from './auth.interceptor';


@NgModule({
  declarations: [
    ValidaComponent,
    ResgistraComponent,
    LoginComponent
  ],
  imports: [
    CommonModule,
    AutenticacaoRoutingModule,
    MaterialModule,
    ReactiveFormsModule
  ]
})
export class AutenticacaoModule { 
  static forRoot(): ModuleWithProviders<AutenticacaoModule> {
    return {
      ngModule: AutenticacaoModule,
      providers: [
        AuthInterceptor
      ]
    }
  }
}
