import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Usuario } from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class AutenticaService {

  readonly url = 'http://localhost:3000/auth';

  private subjUsuario$: BehaviorSubject<Usuario> = new BehaviorSubject<any>([]);
  private subjLoggedIn$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient) { }

  register(user: Usuario): Observable<Usuario> {
    return this.http.post<Usuario>(this.url + '/register', user);
  }


  login(crendentials: {email: string, password: string}): Observable<Usuario> {
    return this.http.post<Usuario>(this.url + '/login', crendentials)
    .pipe(
      tap((u: Usuario)=> {
        localStorage.setItem('token', u.token);
        this.subjLoggedIn$.next(true);
        this.subjUsuario$.next(u);
      })
    )
  }  

  isAuthenticated(): Observable<boolean> {
    let token = localStorage.getItem('token');
    if (token && !this.subjLoggedIn$.value) {
      return this.checkTokenValidation();
    }
    return this.subjLoggedIn$.asObservable();
  }


  checkTokenValidation(): Observable<boolean> {
    return this.http.get<Usuario>(this.url + '/user')
    .pipe(
      tap((u: Usuario)=> {
        if (u) {
          localStorage.setItem('token', u.token);
          this.subjLoggedIn$.next(true);
        this.subjUsuario$.next(u);
        }
      }), 
      map((u: Usuario)=> (u)?true:false),
      catchError((err)=> {
        this.logout();
        return  of(false);
      })
    )
  }

  getUser(): Observable<Usuario> {
    return this.subjUsuario$.asObservable();
  }

  logout() {
    localStorage.removeItem('token');
    this.subjLoggedIn$.next(false);
    this.subjUsuario$.next(null!);
     
  }


}
