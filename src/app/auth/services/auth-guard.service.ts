import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AutenticaService } from 'src/app/autenticacao/services/autentica.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(
    private router: Router,
    private authService: AutenticaService
  ) { }


  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
    ): Observable<boolean> {
       return this.authService.isAuthenticated()
       .pipe(
         tap((b) => {
           if (!b) {
             this.router.navigateByUrl('/autenticacao/login');
           }
         })
       )
    }
}
