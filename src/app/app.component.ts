import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Usuario } from './autenticacao/models/usuario';
import { AutenticaService } from './autenticacao/services/autentica.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'loginApp'

    authenticated$ = new Observable<boolean>();
    user$ = new Observable<Usuario>();

  constructor(
    private autenticaService: AutenticaService,
    private router: Router
    ) {

    this.authenticated$ = this.autenticaService.isAuthenticated();
    this.user$ = this.autenticaService.getUser();

  }

  logout() {
    this.autenticaService.logout();
    this.router.navigateByUrl('autenticacao/login');
  }

}
